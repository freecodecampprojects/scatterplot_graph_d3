$(document).ready(function(){
    //Get Data
    const localData = data;
    const w = 1200;
    const h = 800;
    const p = 100;
    //Create Scale
    const xScale = d3.scaleLinear();
    xScale.domain([d3.min(data, (d) => d["Year"]-1), d3.max(data, (d)=> d["Year"]+1)]);
    xScale.range([p, w-p]);

    const yScale = d3.scaleLinear();
    yScale.domain([d3.min(data, (d) => 
        {
            let date = new Date(0);
            date.setMinutes(parseInt(d["Seconds"]/60));
            date.setSeconds(d["Seconds"]%60);
            return date; 
        })
        ,d3.max(data, (d)=>{
            let date = new Date(0);
            date.setMinutes(parseInt(d["Seconds"]/60));
            date.setSeconds(d["Seconds"]%60);
            return date; 
        })]);
    yScale.range([p, h-p]);
    //Create SVG
    const svg = d3.select(".graph-container")
                .append("svg")
                .attr("width", w)
                .attr("height", h);
    //Create Points
    svg.selectAll("circle")
        .data(data)
        .enter()
        .append("circle")
        .attr("cx", (d) => xScale(d["Year"]))
        .attr("cy", (d) => {
            let date = new Date(0);
            date.setMinutes(parseInt(d["Seconds"]/60));
            date.setSeconds(d["Seconds"]%60);
            return yScale(date)
        })
        .attr("r", 10)
        .attr("stroke", "black")
        .attr("stroke-width", "3")
        .attr("class" , "dot")
        .attr("index", (d,i) => i)
        .attr("data-xvalue", (d)=> d["Year"])
        .attr("data-yvalue", (d)=> {
            let date = new Date(0);
            date.setMinutes(parseInt(d["Seconds"]/60));
            date.setSeconds(d["Seconds"]%60);
            return date
        })
        .style("fill", (d) => {
            return d["Doping"] ? "#0074D9" : "#FF851B";
        })
        .style("color", (d) =>{

        })
    //Create Axis name
    svg.append("text")
        .attr("x", 50)
        .attr("y", 50)
        .attr("id", "y-axis-title")
        .attr("transform", "rotate(-90) translate(-350,0)")
        .text("Time in Minutes")

    //Create Axis
    const axisLeft = d3.axisLeft(yScale).tickFormat(d3.timeFormat("%M:%S"));
    const axisBottom = d3.axisBottom(xScale).tickFormat(d3.format("4"));

    svg.append("g")
        .attr("transform", "translate(0," + (h-p) + ")")
        .attr("id", "x-axis")
        .call(axisBottom);

    svg.append("g")
        .attr("transform", "translate(100,0)")
        .attr("id", "y-axis")
        .call(axisLeft);

    //Create legends
    const legendNoDoping = svg.append("g")
        .attr("id", "legend");
    legendNoDoping.append("text")
        .attr("x", 850)
        .attr("y", 300)
        .text("No doping allegations")
    legendNoDoping.append("rect")
        .attr("x", 1030)
        .attr("y", 280)
        .attr("height", 25)
        .attr("width", 25)
        .attr("fill", "#FF851B");

    const legendDoping = svg.append("g")
        .attr("id", "legend");

    legendDoping.append("text")
        .attr("x", 850)
        .attr("y", 330)
        .text("Doping allegations")
        legendDoping.append("rect")
        .attr("x", 1030)
        .attr("y", 310)
        .attr("height", 25)
        .attr("width", 25)
        .attr("fill", "#0074D9");


    const tooltip = $("#tooltip");
    const nameP = $("#tooltip #name");
    const timeP = $("#tooltip #time");
    const dopingP = $("#tooltip #description");
    //Hover
    $(".dot").hover(function(){
        let index = $(this).attr("index")
        let date = new Date($(this).attr("data-yvalue"));

        tooltip.css("display", "inline-block");
        tooltip.css("top",$(this).position().top + "px");
        tooltip.css("left", ($(this).position().left + 20) + "px");
        tooltip.attr("data-year", localData[index]["Year"]);
        
        nameP.html(localData[index]["Name"] + ": " + localData[index]["Nationality"] )
        timeP.html("Year: " + localData[index]["Year"] + ", Time: " + date.getMinutes() + ":" + date.getSeconds())
        dopingP.html(localData[index]["Doping"])
    
    },function(){
        tooltip.css("display", "none");
    });

});
